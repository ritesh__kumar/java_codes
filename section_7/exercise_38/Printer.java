package section_7.exercise_38;

public class Printer {
    private int tonerLevel, pagesPrinted;
    private boolean duplex;
    public Printer(int tonerLevel, boolean duplex){
        if(tonerLevel <= -1 || tonerLevel >100) this.tonerLevel = -1;
        else this.tonerLevel = tonerLevel;
        this.duplex= duplex;
        this.pagesPrinted=0;
    }
    public int addToner(int tonerAmount) {
        if(tonerAmount > 0 && tonerAmount <=100)
            if((tonerAmount + this.tonerLevel) > 100) return -1;
            else{
                this.tonerLevel+=tonerAmount;
                return this.tonerLevel;
            }
        return -1;
    }
    public int printPages(int pages) {
        int pagesToPrint = pages;
        if(this.duplex){
            System.out.println("Printing in duplex mode");
            this.pagesPrinted+=pagesToPrint/2;
            return (int)Math.ceil(pagesToPrint/(double)2);
        }
        else{
            this.pagesPrinted+=pagesToPrint;
            return pagesToPrint;
        }
    }
    public int getPagesPrinted() {
        return this.pagesPrinted;
    }
}
