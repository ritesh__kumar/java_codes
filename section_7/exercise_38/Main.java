package section_7.exercise_38;

public class Main {
    public static void main(String[] args) {    
        Printer printer = new Printer(50, false);
        System.out.println(printer.addToner(50));
        System.out.println("initial page count = "+ printer.getPagesPrinted());
        int pagesPrinted = printer.printPages(4);
        System.out.println("Pages Printed was "+ pagesPrinted +" new total print count for printer = "+printer.getPagesPrinted());
        pagesPrinted=printer.printPages(2);
        System.out.println("Pages Printed was "+ pagesPrinted +" new total print count for printer = "+printer.getPagesPrinted());
    }
}
