package section_7.exercise_37;

public class Lamp {
    private String style;
    private boolean battery;
    private int globRating;
    public Lamp(String style, boolean battery, int globRating){
        this.battery=battery;
        this.globRating=globRating;
        this.style=style;
    }
    public void turnOn(){
        System.out.println("lamp is being turned on");
    }
    public String getStyle(){
        return this.style;
    }
    public boolean isBattery(){
        return this.battery;
    }
    public int getGlobRating(){
        return this.globRating;
    }
}