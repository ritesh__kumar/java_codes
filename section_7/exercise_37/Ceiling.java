package section_7.exercise_37;

public class Ceiling {
    private int height, paintedColor;
    public Ceiling(int height, int paintedColor){
        this.height=height;
        this.paintedColor=paintedColor;
    }
    public int getPaintedColor(){
        return this.paintedColor;
    }
    public int getHeight(){
        return this.height;
    }
}