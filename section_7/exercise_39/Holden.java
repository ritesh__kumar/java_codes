package section_7.exercise_39;

public class Holden extends Car {
    public Holden(int cylinders, String name) {
        super(cylinders, name);
    }
    @Override
    public String startEngine(){
        return "Holden -> "+this.getName()+"car's engine is starting.";
    }
    @Override
    public String accelerate() {
        return "Holden -> "+this.getName()+ " is accelerating.";
    }
    @Override
    public String brake() {
        return "Holden -> "+this.getName()+" is braking.";
    }
}
