package section_7.exercise_39;

public class Car {
    private boolean engine;
    private String name;
    private int cylinders, wheels;
    public Car(int cylinders, String name) {
        this.name = name;
        this.cylinders = cylinders;
        this.engine=true;
        this.wheels=4;
    }
    public String startEngine(){
        return "Car -> car's engine is starting.";
    }
    public String accelerate() {
        return "the Car -> "+this.getName()+ " is accelerating.";
    }
    public String brake() {
        return "the car Car -> "+this.getName()+" is braking.";
    }
    public String getName(){
        return this.name;
    }
    public int getCylinders(){
        return this.cylinders;
    }
    
}
