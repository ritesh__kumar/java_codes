package section_7.exercise_39;

public class Ford extends Car {
    public Ford(int cylinders, String name) {
        super(cylinders, name);
    }
    @Override
    public String startEngine(){
        return "Ford -> "+this.getName()+"car's engine is starting.";
    }
    @Override
    public String accelerate() {
        return "Ford -> "+this.getName()+ " is accelerating.";
    }
    @Override
    public String brake() {
        return "Ford -> "+this.getName()+" is braking.";
    }
}
