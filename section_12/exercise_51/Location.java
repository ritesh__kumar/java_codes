package section_12.exercise_51;

import java.util.HashMap;
import java.util.Map;

final public class Location {
    final private int locationID;
    final private String description;
    final private Map<String, Integer> exits;

    public Location(int locationID, String description, Map<String, Integer> exits) {
        this.locationID = locationID;
        this.description = description;
        if(exits != null)
            this.exits = new HashMap<String, Integer>(exits);
        else this.exits = new HashMap<>();
        this.exits.put("Q", 0);
    }
    
    public int getLocationID() {
        return locationID;
    }
    
    public String getDescription() {
        return description;
    }

    public Map<String, Integer> getExits() {
        return new HashMap<>(exits);
    }
    
}
