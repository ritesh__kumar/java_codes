package section_12.exercise_52;

import section_10.exercise_52.HeavenlyBody.BodyTypes;

public class DwarfPlanet extends HeavenlyBody {

    public DwarfPlanet(String name, double orbitalPeriod){
        super(name, orbitalPeriod, BodyTypes.DWARF_PLANET);
    }
    
}
