package section_12.exercise_52;

public class Planet extends HeavenlyBody {

    public Planet(String name, double orbitalPeriod){
        super(name, orbitalPeriod, BodyTypes.PLANET);
    }

    @Override
    public boolean addSetellite(HeavenlyBody moon) {
        if(moon.getKey().getBodyType() == BodyTypes.MOON){
            return super.addSetellite(moon);
        }
        return false;
    }


    
}
