package section_12.exercise_52;

import section_10.exercise_52;

public class Moon extends HeavenlyBody {
    public Moon(String name, double orbitalPeriod){
        super(name, orbitalPeriod, BodyTypes.MOON);
    }
}
