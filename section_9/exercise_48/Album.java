package section_9.exercise_48;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private SongList songs;
    
    public Album(String name, String artist){
        this.name = name;
        this.artist = artist;
        this.songs = new SongList();
    }
    public boolean addSong(String title, double duration){
        return this.songs.add(new Song(title, duration));
    }

    public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist){
        Song song = this.songs.findSong(trackNumber);
        if(song == null) return false;
        playlist.add(song);
        return true;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playList){
        Song song = songs.findSong(title);
        if(song == null) return false;
        playList.add(song);
        return true;
    }

    public static class SongList{
        private ArrayList<Song> songs;

        private SongList(){
            this.songs = new ArrayList<>();
        }

        private boolean add(Song song){
            if(songs.contains(song)) return false;
            this.songs.add(song);
            return true;
        }

        private Song findSong(String title){
            for(Song song : this.songs){
                if(song.getTitle().equals(title)) return song;
            }
            return null;
        }

        private Song findSong(int trackNumber){
            int index = trackNumber-1;
            if(index >0 && index < songs.size()) return songs.get(index);
            return null;
        }

    }
}