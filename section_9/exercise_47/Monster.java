package section_9.exercise_47;
import java.util.ArrayList;
import java.util.List;

public class Monster implements ISaveable {
    private String name;
    private int hitPoints;
    private int strength;
    
    public Monster(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
    }

    public String getName() {
        return this.name;
    }

    public int getStrength(){
        return this.strength;
    }

    public int getHitPoints(){
        return this.hitPoints;
    }

    // Monster{name='Werewolf', hitPoints=20, strength=40}

    @Override
    public String toString() {
        return "Monster{"+"name='"+ this.name+'\''+
        ", hitPoints="+ this.hitPoints + 
        ", strength="+ this.strength+
        '}';
    }

    @Override
    public void read(List<String> list) {
        if(list != null && list.size() > 0) {
            this.name = list.get(0);
            this.hitPoints = Integer.parseInt(list.get(1));
            this.strength = Integer.parseInt(list.get(2));
        }
    }

    @Override
    public List<String> write() {
        ArrayList<String> values= new ArrayList<>();
        values.add(0, this.name);
        values.add(1, String.valueOf(this.hitPoints));
        values.add(2, String.valueOf(this.strength));
        
        return values;
    }
}
