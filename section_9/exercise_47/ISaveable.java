package section_9.exercise_47;

import java.util.List;

public interface ISaveable {

    List<String> write();
    void read(List<String> savedValues);
    
}
