package section_9.exercise_49;

public class Node extends ListItem {
    public Node(Object value){
        super(value);
    }

    @Override
    int compareTo(ListItem item) {
        if(item == null) return -1;
        return ((String) super.getValue()).compareTo((String) item.getValue());
    }

    @Override
    ListItem next() {
        return this.rightLink;
    }

    @Override
    ListItem previous() {
        return this.leftLink;
    }

    @Override
    ListItem setNext(ListItem item) {
        this.rightLink = item;
        return this.rightLink;
    }

    @Override
    ListItem setPrevious(ListItem item) {
        this.leftLink = item;
        return this.leftLink;
    }    
}
