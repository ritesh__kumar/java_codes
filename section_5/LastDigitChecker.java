package section_5;

public class LastDigitChecker {
    public static void main(String[] args) {
        int num1=1,num2=1,num3=2;
        System.out.println(isValid(num1)+" "+ isValid(num2)+ " " + isValid(num3));
        System.out.println(hasSameLastDigit(num1, num2, num3));
    }

    public static boolean hasSameLastDigit(int num1, int num2, int num3) {
        if(!isValid(num1) || !isValid(num2) || !isValid(num3)) return false;
        if(num1%10 == num2%10) return true;
        if(num3%10 == num2%10) return true;
        if(num1%10 == num3%10) return true;
        return false;
    }

    public static boolean isValid(int num) {
        if(num < 10 || num > 1000)
            return false;
        return true;
    }
}
