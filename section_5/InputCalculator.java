package section_5;

import java.util.Scanner;

public class InputCalculator {
    public static void main(String[] args) {
        inputThenPrintSumAndAverage();
    }
    public static void inputThenPrintSumAndAverage(){
        Scanner sc = new Scanner(System.in);
        int c=0, sum=0;
        while(true){
            if(sc.hasNextInt()){
                sum+=sc.nextInt();
                c++;
            }
            else{
                sc.close();
                System.out.println("SUM = "+ sum + " AVG = " + Math.round(sum/(double)c));
                break;
            }
        }
    }
}

