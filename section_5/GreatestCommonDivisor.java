package section_5;

public class GreatestCommonDivisor {
    public static void main(String[] args) {
        int first=12, second=30;
        System.out.println(getGreatestCommonDivisor(first, second));
    }

    public static int getGreatestCommonDivisor(int first, int second) {
        if(first < 10 || second < 10) return -1;
        for(int i=Math.min(first, second); i>1; i--){
            if(first%i==0 && second%i==0) return i;
        }
        return 1;
    }
    
}
