package section_5;

public class NoOfDaysInMonth {
    public static void main(String[] args) {
        System.out.println(getDaysInMonth(1, 2020));
    }
    public static boolean isLeapYear(int year){
        if(year <1 || year > 9999) return false;
        if(year % 400 == 0) return true;
        if(year %100 == 0) return false;
        if(year % 4 == 0) return true;
        return false;
    }
    public static int getDaysInMonth(int month, int year){
        if(year < 1 || year > 9999 ||month < 1 || month > 12) return -1;
        if(month==2){
            if(isLeapYear(year)) return 29;
            return 28;
        }
        else{
            if(month==4||month==6||month==9||month==11)
                return 30;
            else
                return 31;
            }
        }
}
    