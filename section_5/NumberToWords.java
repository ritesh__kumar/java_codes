package section_5;

public class NumberToWords {
    public static void main(String[] args) {
        int number=10;
        numberToWords(number);
        System.out.println(reverse(10));
    }

    public static int reverse(int number) {
        String s=Integer.toString(number);
        if(number < 0) s=s.substring(1);
        String rev="";
        for(int i=0; i<s.length(); i++)
            rev=s.charAt(i)+rev;
        if(number <0) return Integer.parseInt("-"+rev);
        return Integer.parseInt(rev);
    }
    public static void numberToWords(int number){
        if(number<0) {
            System.out.println("Invalid Value");
            return;
        }
        String s=Integer.toString(number);
        int i=0;
        while(i<s.length()){
            
            switch (s.charAt(i)) {
                case '0':
                    System.out.println("Zero");
                    break;
                case '1':
                    System.out.println("One");
                    break;
                case '2':
                    System.out.println("Two");
                    break;
                case '3':
                    System.out.println("Three");
                    break;
                case '4':
                    System.out.println("Four");
                    break;
                case '5':
                    System.out.println("Five");
                    break;
                case '6':
                    System.out.println("Six");
                    break;
                case '7':
                    System.out.println("Seven");
                    break;
                case '8':
                    System.out.println("Eight");
                    break;
                case '9':
                    System.out.println("Nine");
                    break;
            }
            i++;
        }
    }
    public static int getDigitCount(int number) {
        int c=0;
        if( number==0) return 1;
        if(number<0) return -1;
        while(number>0){
            c++;
            number/=10;
        }
        return c;
    }
}
