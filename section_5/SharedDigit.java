package section_5;

public class SharedDigit {
    public static void main(String[] args) {
        int num1=12, num2=43;
        System.out.println(hasSharedDigit(num1, num2));
    }
    public static boolean hasSharedDigit(int x, int y){
        if((10 > x || x > 99) || (10 > y || y > 99)) return false;
        int[] arr1 = new int[10];
        int[] arr2 = new int[10];
        while(x > 0){
            arr1[x%10]=1;
            x=x/10;
        }
        while(y > 0){
            arr2[y%10]=1;
            y=y/10;
        }
        for(int i=0; i<10; i++)
            if(arr1[i] == arr2[i] && arr1[i]==1) return true;
        return false;
    }
    
}
