package section_5;

public class EvenDigitSum {
    public static void main(String[] args) {
        System.out.println(getEvenDigitSum(252));
    }

    public static int getEvenDigitSum(int num) {
        if(num < 0) return -1;
        int sum=0;
        int n=num;
        while(n>0){
            int rem=n%10;
            if(rem%2==0) sum+=rem;
            n=n/10;
        }
        return sum;
    }
    
}
