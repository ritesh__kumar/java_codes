package section_5;

public class PerfectNumber {
    public static void main(String[] args) {
        int num=6;
        System.out.println(isPerfectNumber(num));
    }

    public static boolean isPerfectNumber(int num) {
        if(num < 1) return false;
        int sum=0,i=1;
        while(i<num){
            if(num%i==0) sum+=i;
            i++;
        }
        if(sum==num) return true;
        return false;
    }
}
