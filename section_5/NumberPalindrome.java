package section_5;

public class NumberPalindrome {
    public static boolean isPalindrome(int number){
        String s=Integer.toString(number);
        if(s.charAt(0)=='-') s=s.substring(1);
        int len=s.length();
        for(int i=0; i<len/2; i++)
            if(s.charAt(i) != s.charAt(len-i-1)) return false;
        return true;
    }

}