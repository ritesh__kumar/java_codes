package section_8.exercise_43;

import java.util.Arrays;
import java.util.Scanner;

public class ReverseArray {
    private static void reverse(int[] arr){
        System.out.print("Array = "+ Arrays.toString(arr));
        int i=0, j=arr.length-1;
        while(i < j){
            int temp=arr[i];
            arr[i]=arr[j];
            arr[j]=temp;
            i++;
            j--;
        }
        System.out.print(" Reversed array = "+ Arrays.toString(arr));
        
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int size=sc.nextInt();
        int[] arr = new int[size];
        for(int i=0; i<size; i++)
            arr[i]=sc.nextInt();
        sc.close();
        reverse(arr);
        
    }
}
