package section_8.exercise_46;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String albumName, String artist){
        this.name=albumName;
        this.artist=artist;
        this.songs= new ArrayList<>();
    }
//-  addSong(), has two parameters of type String (title of song), double (duration of song) and returns a boolean. 
//   Returns true if the song was added successfully or false otherwise.

    public boolean addSong(String title, double duration) {
        if(findSong(title) == null){
            this.songs.add(new Song(title, duration));
            return true;
        }
        return false;
    }

//    -  findSong(), has one parameter of type String (title of song) and returns a Song.
//       Returns the Song if it exists, null if it doesn't exists.


    private Song findSong(String title){
        for(Song song : this.songs){
            if(song.getTitle().equals(title)) return song;
        }
        return null;
    }

//      -  addToPlayList(), has two parameters of type int (track number of song in album) and LinkedList (the playlist) 
//         that holds objects of type Song, and returns a boolean.
//         Returns true if it exists and it was added successfully using the track number, or false otherwise.

    public boolean addToPlayList(int trackNumber, LinkedList<Song> playList) {
        int index=trackNumber-1;
        if(index >= 0 && index <= this.songs.size()){
            playList.add(this.songs.get(index));
            return true;
        }
        return false;
    }

//        -  addToPlayList(), has two parameters of type String (title of song) and LinkedList (the playlist) that holds objects of type Song, and returns a boolean.
//         Returns true if it exists and it was added successfully using the name of the song, or false otherwise.
    public boolean addToPlayList(String titleName, LinkedList<Song> playList) {
        if(findSong(titleName) != null){
            playList.add(findSong(titleName));
            return true;
        }
        return false;
    }
}

