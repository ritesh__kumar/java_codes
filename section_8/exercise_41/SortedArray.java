package section_8.exercise_41;

import java.util.Scanner;

public class SortedArray {
    public static int[] getIntegers(int size){
        Scanner sc = new Scanner(System.in);
        int[] arr = new int[size];
        for(int i=0; i<size; i++)
            arr[i]=sc.nextInt();
        sc.close();
        return arr;
    }
    public static void printArray(int[] arr){
        for(int i=0; i<arr.length; i++)
            System.out.println("Element "+i+" contents " +arr[i]);
    }
    public static int[] sortIntegers(int[] arr){
        for(int i=0; i<arr.length; i++)
            for(int j=i; j<arr.length; j++)
                if(arr[i] < arr[j]){
                    int temp=arr[j];
                    arr[j]=arr[i];
                    arr[i]=temp;
                }
        return arr;
    }
}
