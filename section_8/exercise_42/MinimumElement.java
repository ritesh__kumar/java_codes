package section_8.exercise_42;

import java.util.Scanner;

public class MinimumElement {
    private static int readInteger(){
        Scanner scanner=new Scanner(System.in);
        int x=scanner.nextInt();
        scanner.close();
        return x;
    }
    private static int[] readElements(int size){
        int[] arr = new int[size];
        Scanner scanner=new Scanner(System.in);
        int i=0;
        while(i<size){
            arr[i]=scanner.nextInt();
            i++;
        }
        scanner.close();
        return arr;
    }
    private static int findMin(int[] arr){
        int min=Integer.MAX_VALUE;
        for(int i: arr){
            if(min > i) min=i;
        }
        return min;
    } 
}