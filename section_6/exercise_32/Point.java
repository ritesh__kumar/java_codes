package section_6.exercise_32;

public class Point {
    private int x,y;

    public Point(int x, int y) {
        this.x=x;
        this.y=y;
    }

    public Point() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance(int x, int y) {
        double dist=Math.sqrt((this.x-x)*(this.x-x) + (this.y-y)*(this.y-y));
        return dist;
    }

    public double distance() {
        double dist=Math.sqrt((this.x-0)*(this.x-0) + (this.y-0)*(this.y-0));
        return dist;
    }

    public double distance(Point second) {
        double dist=Math.sqrt((second.x-this.x)*(second.x-this.x) + (second.y-this.y)*(second.y-this.y));
        return dist;
    }

}
