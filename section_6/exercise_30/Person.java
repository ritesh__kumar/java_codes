package section_6.exercise_30;

public class Person {
    private String firstName, lastName;
    private int age;
    public String getFirstName(){
        return this.firstName;
    }
    public String getLastName(){
        return this.lastName;
    }
    public int getAge(){
        return this.age;
    }
    public void setFirstName(String first){
        this.firstName = first;
    }
    public void setLastName(String last){
        this.lastName = last;
    }
    public void setAge(int AGE){
        if(AGE <0 || AGE > 100) this.age=0;
        else this.age = AGE;
    }
    public boolean isTeen(){
        if(this.age > 12 && this.age < 20) return true;
        return false;
    }
    public String getFullName(){
        if(this.firstName.isEmpty() && this.lastName.isEmpty()) return "";
        if(this.firstName.isEmpty() && !this.lastName.isEmpty()) return this.lastName;
        if(!this.firstName.isEmpty() && this.lastName.isEmpty()) return this.firstName;
        return this.firstName+" "+this.lastName;
    }
}