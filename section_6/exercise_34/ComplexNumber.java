package section_6.exercise_34;

public class ComplexNumber {
    private double real, imaginary;
    public ComplexNumber(double real, double imaginary){
        this.imaginary=imaginary;
        this.real=real;
    }
    public double getReal(){
        return this.real;
    }
    public double getImaginary(){
        return this.imaginary;
    }
    public void add(double real, double imaginary){
        this.real+=real;
        this.imaginary+=imaginary;
    }
    public void add(ComplexNumber num){
        this.real+=num.real;
        this.imaginary+=num.imaginary;
    }
    public void subtract(double real, double imaginary){
        this.real-=real;
        this.imaginary-=imaginary;
    }
    public void subtract(ComplexNumber num){
        this.real-=num.real;
        this.imaginary-=num.imaginary;
    }
}
