package section_4;

import java.util.Scanner;

public class Speed_Converter{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double kilometersPerHour = sc.nextDouble();
        sc.close();
        printConversion(kilometersPerHour);
    }
    public static long toMilesPerHour(double kilometersPerHour){
        if(kilometersPerHour < 0) return -1l;

        return (long)Math.round(kilometersPerHour/1.609);
    }
    public static void printConversion(double kilometersPerHour){
        if(kilometersPerHour < 0) System.out.println("Invalid Value");
        else System.out.println(kilometersPerHour + " km/h = " + toMilesPerHour(kilometersPerHour) + " mi/h");
    }
}