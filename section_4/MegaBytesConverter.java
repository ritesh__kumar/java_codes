package section_4;

public class MegaBytesConverter {
    public static void printMegaBytesAndKiloBytes(int kilobytes){
        if(kilobytes < 0) System.out.println("Invalid Output");
        else{
            System.out.println(kilobytes + " KB = " + kilobytes/1024 + " MB and "+ kilobytes%1024 + " KB");
        }
    }
    public static void main(String[] args) {
        printMegaBytesAndKiloBytes(2500);
    }
}